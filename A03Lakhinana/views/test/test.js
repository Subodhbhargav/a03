QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});
QUnit.test('Testing  number of what percentage in the given number ', function (assert) {
    // assert.equal(convertTOCelcius(30), 86,303,545,105,9, "works with positive integers");
    assert.equal(test(-10,-100), 1000, "works with a negative number");
    assert.throws(function() { test(NaN) }, 'Given is not a number');
    assert.throws(function() { test(null) }, 'Given is not a number');
    assert.throws(function() { test("abc") }, 'Given is not a number');
    assert.throws(function() { test(undefined) }, 'Given is not a number');
});